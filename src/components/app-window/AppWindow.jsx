import React from 'react';
import Modal from 'react-modal';

Modal.setAppElement("#root")

const AppWindow = ({ show, onClose, item, indicator }) => {
    return (
        <Modal isOpen={show} onRequestClose={onClose} className="modal" overlayClassName="overlay">
            <div className="top-wrapper">
                <div className="close-button-wrapper">
                    <button className="close-button" onClick={onClose}>x</button>
                </div>
                <div className="top-content">
                    {/* <div className="status-wrapper">
                        <p className="status">{item.status}</p>
                    </div> */}
                    <div className="title-wrapper">
                        <p className="modal-title">{item.title}</p>
                        <div className={`status ${indicator}`}>In Status: <span>{item.status}</span></div>
                    </div>
                </div>
             </div>
            <div className="bottom-content">
                <p className="description">{item.content}</p>
            </div>
        </Modal>
    )
}

export default AppWindow
