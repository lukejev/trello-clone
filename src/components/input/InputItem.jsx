import React, {useState} from 'react'

const InputItem = ({ className, addItem }) => {

    const [title, setTitle] = useState('');
    const [content, setContent] = useState('');



    const addCard = (e) => {
        e.preventDefault();
        const titleValue = title;
        const contentValue = content;

        const cardData = {
            status: 'To Do',
            title: titleValue,
            content: contentValue
        };

        if (titleValue !== '' && contentValue !== '') {
            addItem(cardData)
        }

    };

    return (
        <div className={className}>
            <form onSubmit={addCard}>
                <input type="text" className="input-field title" placeholder="Title" onChange={e => setTitle(e.target.value)}></input>
                <input type="text" className="input-field content" placeholder="Content" onChange={e => setContent(e.target.value)}></input>
                <button type="submit-button" className="submit-button" value="submit">Submit</button>
            </form>
        </div>
    )
}

export default InputItem



