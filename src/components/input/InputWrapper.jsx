import React, {useState, Fragment} from 'react';
import InputItem from './InputItem';

const InputWrapper = ({ addItem }) => {

    const [open, setOpen] = useState(false);

    const addAndClose = (item) => {
        addItem(item);
        setOpen(false);
    }

    return (
        <Fragment>
            <div className="input-wrapper" onClick={() => setOpen(!open)}>
                <button className="input-button">{open ? 'Cancel ' : 'Add +'}</button>
            </div>
            {open ?
                <InputItem addItem={addAndClose} className={`input-item ${open ? 'show' : 'hide'}`} />
            : 
                '' 
            }
        </Fragment>
    )
}

export default InputWrapper;

// on click of the add button render an input, 