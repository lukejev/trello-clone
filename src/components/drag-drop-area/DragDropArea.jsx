import React from 'react';
import { useDrop } from 'react-dnd';
import { statuses } from '../../data/data'
import itemType from '../../data/types';

const DragDropArea = ({ onDrop, children, status }) => {

    // used to find index of card in column 
    const [{ isOver }, drop] = useDrop({
        accept: itemType,
        canDrop: (item, monitor) => {
            const itemIndex = statuses.findIndex(statusIcon => statusIcon.status === item.status);
            const statusIndex = statuses.findIndex(statusIcon => statusIcon.status === status);
            // looks for 1 > || 1 < || === and returns boolean to follow a process
            return [itemIndex + 1, itemIndex -1, itemIndex].includes(statusIndex);
        },
        drop: (item, monitor) => {
            onDrop(item, monitor, status);
        },
        collect: monitor => ({
            isOver: monitor.isOver()
        })
    });

    // pass everything as a child with cloneElement
    return (
        <div ref={drop} className="drag-drop-area">
            {React.cloneElement(children, { isOver })}
        </div>
    )
}

export default DragDropArea;
