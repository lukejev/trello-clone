import React from 'react'

const Header = () => {
    return (
        <div className="header">
            <div className="content">
                <h1>Trello's Second Cousin</h1>  
            </div>
        </div>
    )
}

export default Header
