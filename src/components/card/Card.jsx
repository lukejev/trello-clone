import React, { Fragment, useState, useRef} from 'react';
import { useDrag, useDrop } from 'react-dnd';
import AppWindow from '../app-window/AppWindow';
import itemType from '../../data/types';

const Card = ({ item, index, moveItem, status, indicator, deleteItem }) => {

    const ref = useRef(null);

    const [, drop] = useDrop({
        accept: itemType,
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }

            const dragIndex = item.index;
            const hoverIndex = index;

            if (dragIndex === hoverIndex) {
                return;
            }

            const hoveredRect = ref.current.getBoundingClientRect();
            const hoverMiddleY = (hoveredRect.bottom - hoveredRect.top) / 2;
            const mousePosition = monitor.getClientOffset();
            const hoverClientY = mousePosition.y - hoveredRect.top;

            // travel down check
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }

            // travel up check
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }

            moveItem(dragIndex, hoverIndex);
            item.index = hoverIndex;
        }

    })

    const [{ isDragging }, drag] = useDrag({
        item: {type: itemType, ...item, index},
        collect: monitor => ({
            isDragging: monitor.isDragging()
        })
    });

    const [show, setShow] = useState(false);

    const onOpen = () => setShow(true);

    const onClose = () => setShow(false);

    drag(drop(ref));

    return (
        <Fragment>
            <div className="card" ref={ref} style={{ opacity: isDragging ? 0 : 1 }} >
                <div className="card-content" onClick={onOpen}>
                    <p className="title">{item.title}</p>
                    <div className={`status ${indicator}`}></div>
                </div>
                <button className="card-delete" onClick={() => deleteItem(item)}>x</button>
            </div>
            <AppWindow 
                indicator={indicator}
                item={item}
                onClose={onClose}
                show={show}
            /> 
        </Fragment>
    )
}

export default Card

// colours
// main background for card, #ffffff
// main background for kanban, #f7f8fa
// dark grey, maybe good for header, #3d3d3d
// to do, #96beff
// in progress, #ff79a8
// qa, #a381ff
// done, #91deaf