import { Fragment } from "react"
import React from 'react'
import InputWrapper from '../input/InputWrapper';

const Column = ({ isOver, children, addItem, status }) => {

    const className = isOver ? "highlight-region" : "";

    return (
        <Fragment>
            {status === "To Do" ? <InputWrapper addItem={addItem}/> : ''}
            <div className={`column${className}`}>
                {children}
            </div>
            {status === "To Do" ? <InputWrapper addItem={addItem}/> : ''}
        </Fragment>
    )
}

export default Column
