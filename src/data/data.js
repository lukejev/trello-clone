const data = [{
    id: 1,
    icon: "⭕️",
    status: "To Do",
    title: "Add mobile breakpoint to designs",
    content: "Add designs for mobile devices at 768px breakpoint"
}, {
    id: 2,
    icon: "⭕️",
    status: "To Do",
    title: "Image not rendering in hero banner",
    content: "Ensure that the hero banner image renders correctly across supported browsers"
}, {
    id: 3,
    icon: "⭕️",
    status: "To Do",
    title: "Update disclaimer content",
    content: "See spec sheet for new content"
}];

const statuses = [{
    status: "To Do",
    style: "todo",
    icon: "⭕"
}, {
    status: "In Progress",
    style: "inprogress",
    icon: "🏃"
}, {
    status: "QA",
    style: "qa",
    icon: "👀"
}, {
    status: "Done",
    style: "done",
    icon: "✅"
}];


export { data, statuses };