import React, {useState} from 'react'
import Card from '../components/card/Card';
import DragDropArea from '../components/drag-drop-area/DragDropArea';
import Column from '../components/column/Column';
import { data, statuses } from '../data/data';
import { v4 as uuidv4 } from 'uuid';

// could inlcude validation function to check for string

const Kanban = () => {

    // local storage

    // const [storedData, saveData] = useState(
    //     localStorage.getItem('saved data' || data)
    // );

    // useEffect(() => {
    //     localStorage.setItem('saved data', storedData);
    // }, [storedData]);

    const [items, setItems] = useState(data);

    const addItem = (item) => {
        // add item to array, call setItems on that array
        // becomes mutable, will modify the existing data set rather than creating a new one
        const dataSet = {
            ...item,
            id: uuidv4()
        }
        setItems([
            ...items, 
            dataSet
        ])        
    }

    const deleteItem = (item) => {
        const newItems = items.filter(i => i.id !== item.id)
        setItems(newItems);
    }

    const onDrop = (item, monitor, status) => {
        const statusMap = statuses.find(statusIcon => statusIcon.status === status);

        setItems(prevState => {
            const newItems = prevState
                .filter(i => i.id !== item.id)
                .concat({...item, status, icon: statusMap.icon});
                return [ ...newItems ];
        });
    };

    // filter out and insert new

    const moveItem = (dragIndex, hoverIndex) => {
        const item = items[dragIndex];
        setItems(prevState => {
            const newItems = prevState.filter((i, idx) => idx !== dragIndex);
            newItems.splice(hoverIndex, 0, item);
            return [ ...newItems ];
        });
    };

    // create columns and status names

    return (
        <div className="row">
            {statuses.map(s => {
                return (
                    <div className="column-container" key={s.status}>
                        <h2 className="column-title">{s.status}</h2>
                        <DragDropArea onDrop={onDrop} status={s.status}>
                            <Column status={s.status} addItem={addItem}>
                                {items
                                    .filter(i => i.status === s.status)
                                    .map((i, idx) => <Card key={i.id} item={i} index={idx} moveItem={moveItem} deleteItem={deleteItem} status={s} indicator={s.style} />)
                                }
                            </Column>
                        </DragDropArea>
                    </div>
                );
            })}
        </div>
    )
}

export default Kanban

