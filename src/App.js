import React from 'react';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend'

import Kanban from './screens/Kanban';
import Header from './components/header/Header';

import './base-styles/base.scss';


function App() {
  return (
    <DndProvider backend={HTML5Backend}>
        <Header />
        <Kanban />
    </DndProvider>

  );
}

export default App;
